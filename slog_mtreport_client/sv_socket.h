/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   模块 slog_mtreport_client 功能:
        用于上报除监控系统本身产生的监控点数据、日志，为减少部署上的依赖
		未引入任何第三方组件

****/

#ifndef __SV_SOCKET__
#define __SV_SOCKET__ (1)

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h> 
#include <arpa/inet.h>
#include <iostream>
#include "sv_struct.h"

#define MT_SOCKET_DEF_COUNT_MAX  100
#define MT_SOCKET_DEF_SELECT_TIME_US 10000

struct MtSocket;
struct MtReportSocket;
typedef void (* OnPkg_f)(struct MtSocket *psock, char * sBuf, int iLen, time_t uiTime);
typedef void (* OnTimeout_f)(time_t uiTime);
typedef void (* OnError_f)(struct MtSocket *pstSock, time_t uiTime);

class CPacketInfo 
{
#define UDP_RETRY_TIME_SECONDS 80
#define UDP_RETRY_PACKS 5

    struct _PackInfo {
        bool bServerHasAck; 

        uint32_t dwLastAddTime; // 最新数据包添加时间, 可用于回收长期不使用的节点
        uint32_t dwRemoteAddr; // 服务器地址
        uint16_t wRemotePort;
        uint32_t dwReqCmd; // 命令号
        uint32_t dwRetryStartTime; // 可靠udp开启时间, 用于网络状况恢复时可禁用可靠udp机制
        int32_t iSendPacks; // 已发送udp 包量
        int32_t iRecvAckCur; // 当前已接收 ack 包量

        _PackInfo() {
            Reset();
        }
        void Reset() {
            dwLastAddTime=0;
            dwRemoteAddr=0;
            wRemotePort=0;
            dwReqCmd =0;
            dwRetryStartTime=0;
            iSendPacks=0;
            bServerHasAck=false;
        }
        bool IsEqual(uint32_t dwAddr, uint16_t wPort, uint32_t Cmd) {
            return (dwRemoteAddr==dwAddr && wRemotePort==wPort && dwReqCmd==Cmd);
        }
        bool IsFree();
    };

    public:
        CPacketInfo() {}
        int GetPackInfoIndex(uint32_t cmd, struct sockaddr_in *pDestAddr, int *pNewIdx);
        void InitPackInfo(uint32_t cmd, struct sockaddr_in *pDestAddr, int iPackIdx);
        bool NeedRetry(ReqPkgHead *pHead, int iPackIdx);
        int GetSendPacks(int iPackIdx) {
            return m_pkInfo[iPackIdx].iSendPacks;
        }
        void AddSendPacks(int iPackIdx);
        void AddRecvAck(int iPackIdx);
        bool StartUdpRetry(int iPackIdx, bool bForce);
        bool IsPkgNeedRetry(char *pkg) {
            if(((ReqPkgHead*)(pkg+1))->cNeedAck)
                return true;
            return false;
        }
        void OutPackInfo();
        bool IsServerHasAck(int iPackIdx) {
            if(iPackIdx >= 0 && iPackIdx < MT_SOCKET_DEF_COUNT_MAX)
                return m_pkInfo[iPackIdx].bServerHasAck;
            return false;
        }

    private:
        _PackInfo m_pkInfo[MT_SOCKET_DEF_COUNT_MAX];
};

class CircularBuffer
{   
    public:
        CircularBuffer(size_t size);
        ~CircularBuffer();

        bool Write(const char *p,size_t l); 
        bool Read(char *dest,size_t l); 
        bool Remove(size_t l); 
        std::string ReadString(size_t l); 

        size_t GetLength();
        const char *GetStart();
        size_t GetL();
        size_t Space();
        unsigned long ByteCounter(bool clear = false);

    private:
        CircularBuffer(const CircularBuffer& s) {}
        CircularBuffer& operator=(const CircularBuffer& ) { return *this; }
        char *buf;
        size_t m_max;
        size_t m_q;
        size_t m_b;
        size_t m_t;
        unsigned long m_count;
};




struct MtSocket
{
    void Reset() {
        isock = 0;
        iMaxRespTimeMs = 0;
        memset(&remote, 0, sizeof(remote));
        memset(&last_recv_remote, 0, sizeof(last_recv_remote));
        buf = NULL;
        pcycbuf = NULL;
        onPkg = NULL;
        iPrivLen = 0;
    }

	int isock;
	int iMaxRespTimeMs; // 该 socket 远端服务器的最大响应时间
	struct sockaddr_in remote;
	struct sockaddr_in last_recv_remote;
	char *buf;
    CircularBuffer *pcycbuf;
	OnPkg_f onPkg;

    char sPrivBuf[128];
    int iPrivLen;
};

enum {
    REMOTE_PROTOC_TCP=1,
    REMOTE_PROTOC_UDP=2
};


#define MTSOCKET_TO_INDEX(psock) (((unsigned char*)psock)-((unsigned char*)g_socket.socks))/sizeof(MtSocket)
struct MtReportSocket
{
    MtReportSocket():iRemoteProtoc(0), cIsInit(0), iNumSock(0), iLastFreeIdx(0), iSockMax(0)
    {
        FD_ZERO(&socks_save);
        FD_ZERO(&socks_use);
        onTimeout = NULL;
        onError = NULL;
    }

    int iRemoteProtoc;
	char cIsInit;
	int iNumSock;
	int iLastFreeIdx;
	struct MtSocket socks[MT_SOCKET_DEF_COUNT_MAX];
	int iSockMax;
	fd_set socks_save;
	fd_set socks_use;
	OnTimeout_f onTimeout;
	OnError_f onError;
    CPacketInfo pack;
};

extern struct MtReportSocket g_socket;

int InitSocket(OnTimeout_f onTimeout, OnError_f onError, int iProtoc);
int MySocket(int flag);
int AddSocket(int isock, struct sockaddr_in *paddr, OnPkg_f onPkg, char *priv=NULL, int iPrivLen=0);
int CheckSocket(time_t tmcur, uint32_t usSelect);
int SendPacket(int iSockIdx, struct sockaddr_in *pDestAddr, const char *pdata, int iDataLen, bool bEnableUdpAutoRetry=false);
int SendAckPacket(struct MtSocket *psock, const char *pdata, int iDataLen);
uint32_t GetSocketAddress(int iSockIdx);
void SetSocketAddress(int iSockIdx, uint32_t dwNetAddress, uint16_t wPort=0);
void InitSocketComm(int iSock);
int32_t GetMaxResponseTime(int iSockIdx);
void ModMaxResponseTime(int iSockIdx, int iRespTime);

#endif

